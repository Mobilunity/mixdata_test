(function() {
    'use strict';

    angular
        .module('app.services', [])
        .service('authService', function($q){

            var logData = {
                name: 'admin',
                mail: 'admin@mail.com',
                pass: '1234'
            };

            return {

                validationName: function validationName(username, password) {
                    var q = $q.defer();

                    setTimeout(function() {
                        if (username === logData.name && password == logData.pass) {
                            q.resolve('positive');
                        }
                        else if (username === logData.mail && password == logData.pass) {
                            q.resolve('positive');
                        }
                        else {
                            q.reject('negative');
                        }
                    }, 100);

                    return q.promise;
                },

                forgotValid: function forgotValid(username) {
                    var q = $q.defer();

                    setTimeout(function() {
                        if (username === logData.name) {
                            q.resolve('positive');
                        }
                        else if (username === logData.mail) {
                            q.resolve('positive');
                        }
                        else {
                            q.reject('negative');
                        }
                    }, 100);

                    return q.promise;
                },

                saveChange: function saveChange(passNew, passRepeat) {
                    var q = $q.defer();

                    setTimeout(function() {
                        if (passNew === passRepeat && passNew != undefined && passRepeat != undefined) {
                            logData.pass = passNew;
                            q.resolve('positive');
                        }
                        else {
                            q.reject('negative');
                        }
                    }, 100);

                    return q.promise;
                }
            };
        })
})();

