(function() {
    'use strict';

    angular
        .module('app.login', [])

        .config( function($routeProvider, $locationProvider) {

            $routeProvider.when('/', {
                templateUrl: 'app/templates/components/login.html',
                controller: 'login',
                controllerAs: 'login'
            });

            $routeProvider.when('/success', {
                templateUrl: 'app/templates/components/success.html',
                controller: 'login',
                controllerAs: 'login'
            });

            $locationProvider.html5Mode(true);

        })

        .controller('login', ['authService','$location', function (authService, $location) {
            var vm = this ;

            vm.validationName = validationName;
            function validationName(name, pass){
                authService.validationName(name, pass)
                    .then(function(result) {
                        $location.path('/success')
                    })
                    .catch(function(result) {
                        vm.errorLog = true;
                    })
            }

        }]);
})();
