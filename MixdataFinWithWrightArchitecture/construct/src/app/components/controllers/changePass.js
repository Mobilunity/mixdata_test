(function() {
    'use strict';

    angular
        .module('app.changePass', [])

        .config( function($routeProvider, $locationProvider) {

            $routeProvider.when('/password_change', {
                templateUrl: 'app/templates/components/password_change.html',
                controller: 'changePass',
                controllerAs: 'changePass'
            });

            $locationProvider.html5Mode(true);

        })

        .controller('changePass', ['authService','$location', function (authService, $location) {
            var vm = this ;

            vm.saveChange = saveChange;
            function saveChange(passNew, passRepeat){
                authService.saveChange(passNew, passRepeat)
                    .then(function(result) {
                        $location.path('/success')
                    })
                    .catch(function(result) {
                        vm.repeatPasswordCheck = true;
                    })
            }

        }])
})();
