(function() {
    'use strict';

    angular
        .module('app.forgotPass', [])

        .config( function($routeProvider, $locationProvider) {

            $routeProvider.when('/forgot_pass', {
                templateUrl: 'app/templates/components/forgot_pass.html',
                controller: 'forgotPass',
                controllerAs: 'forgotPass'
            });

            $locationProvider.html5Mode(true);

        })

        .controller('forgotPass', ['authService','$location', function (authService, $location) {
            var vm = this ;

            vm.forgotValid = forgotValid;
            function forgotValid(username){
                authService.forgotValid(username)
                    .then(function(result) {
                        $location.path('/password_change')
                    })
                    .catch(function(result) {
                        vm.validName = true;
                    })
            }

        }])
})();
