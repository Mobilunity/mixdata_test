'use strict';
/*__________________________________________________advertisement_variables*/

var gulp = require('gulp');
var watch = require('gulp-watch');
var prefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var sourcemaps =require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var pngquant = require('imagemin-pngquant');
var rimraf = require('rimraf');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var rigger = require('gulp-rigger');
var inject = require('gulp-inject');
// var mainBowerFiles = require('main-bower-files');
var multiDest = require('gulp-multi-dest');
var reload = browserSync.reload;
var gcallback = require('gulp-callback');

/*-------------------------------------------------------------------------*/

var path = {
    build: {
        html: 'dist/src/',
        templatesHtml: 'dist/src/app/templates/',
        js: 'dist/src/app/',
        css: 'dist/src/app/styles',
        preCss: 'construct/src/app/styles/css',
        img: 'dist/src/assest/images/',
        fonts: 'dist/src/assest/fonts/'
    },

    src: {
        html: 'construct/src/*.html',
        templatesHtml: 'construct/src/app/templates/**/*.html',
        js: 'construct/src/app/**/*.*',
        style: 'construct/src/app/styles/scss/styles.scss',
        grid: 'construct/src/app/styles/css/*.css',
        img: 'construct/src/assest/images/**/*.*',
        fonts: 'construct/src/assest/fonts/**/*.*'
    },

    watch: {
        html: 'construct/src/*.html',
        templatesHtml: 'construct/src/app/templates/**/*.html',
        js: 'construct/src/app/**/*.*',
        style: 'construct/src/app/styles/scss/styles.scss',
        grid: 'construct/src/app/styles/css/*.css',
        img: 'construct/src/assest/images/**/*.*',
        fonts: 'construct/src/assest/fonts/**/*.*'
    },
    clean: './dist'
};

var config = {
    server: {
        baseDir: "./dist/src/"
    },
    host: 'localhost',
    port: 4001,
    logPrefix: "Frontend_Project"
};

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(gcallback(function() {
            gulp.src(path.src.html)
                .pipe(inject(gulp.src(['construct/src/app/**/*.js', 'construct/src/app/styles/css/**/*.css'], {read: false}), { ignorePath: 'construct/src/', addRootSlash: false }))
                .pipe(gulp.dest('construct/src/'));
            }))
        .pipe(reload({stream: true}));
});

gulp.task('templatesHtml:build', function () {
    gulp.src(path.src.templatesHtml)
        .pipe(gulp.dest(path.build.templatesHtml))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer({
            browsers: ['last 10 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(multiDest([path.build.preCss, path.build.css]))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

// gulp.task('bower:build', function () {
//     return gulp.src('dist/index.html')
//         .pipe(inject(gulp.src(mainBowerFiles(), {read: false}), {name: 'bower', relative: true}))
//         .pipe(gulp.dest('dist/'));
// });

// gulp.task('refs:build', function() {
//     gulp.src(path.src.html)
//         .pipe(inject(gulp.src(['construct/src/app/**/*.js', 'construct/src/app/styles/css/**/*.css'], {read: false}), { ignorePath: 'construct/src/', addRootSlash: false }))
//         .pipe(gulp.dest('construct/src/'));
// });

gulp.task('grid:build', function() {
    gulp.src(path.src.grid)
        .pipe(gulp.dest(path.build.css));
});

gulp.task('build', [
    'js:build',
    'style:build',
    'templatesHtml:build',
    'html:build',
    'grid:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);

