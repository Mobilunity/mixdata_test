(function() {
    'use strict';

    angular.module('app', ['ngRoute', 'ngMessages','app.services', 'app.login', 'app.forgotPass', 'app.changePass'])

})();

