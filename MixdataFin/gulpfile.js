'use strict';
/*__________________________________________________advertisement_variables*/

var gulp = require('gulp');
var watch = require('gulp-watch');
var prefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var sourcemaps =require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var pngquant = require('imagemin-pngquant');
var rimraf = require('rimraf');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var rigger = require('gulp-rigger');
var inject = require('gulp-inject');
// var mainBowerFiles = require('main-bower-files');
var multiDest = require('gulp-multi-dest');
var reload = browserSync.reload;

/*-------------------------------------------------------------------------*/

var path = {
    build: {
        html: 'dist/',
        templatesHtml: 'dist/templates/',
        js: 'dist/js/',
        css: 'dist/css/',
        preCss: 'app/css',
        img: 'dist/images/',
        fonts: 'dist/fonts/'
    },

    src: {
        html: 'app/*.html',
        templatesHtml: 'app/templates/**/*.html',
        js: 'app/js/**/*.*',
        style: 'app/scss/styles.scss',
        grid: 'app/css/*.css',
        img: 'app/images/**/*.*',
        fonts: 'app/fonts/**/*.*'
    },

    watch: {
        html: 'app/**/*.html',
        js: 'app/js/**/*.js',
        style: 'app/scss/**/*.scss',
        grid: 'app/css/*.css',
        img: 'app/images/**/*.*',
        fonts: 'app/fonts/**/*.*'
    },
    clean: './dist'
};

var config = {
    server: {
        baseDir: "./dist"
    },
    host: 'localhost',
    port: 4001,
    logPrefix: "Frontend_Project"
};

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('templatesHtml:build', function () {
    gulp.src(path.src.templatesHtml)
        .pipe(gulp.dest(path.build.templatesHtml))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer({
            browsers: ['last 10 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(multiDest([path.build.preCss, path.build.css]))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

// gulp.task('bower:build', function () {
//     return gulp.src('dist/index.html')
//         .pipe(inject(gulp.src(mainBowerFiles(), {read: false}), {name: 'bower', relative: true}))
//         .pipe(gulp.dest('dist/'));
// });

gulp.task('refs:build', function() {
    gulp.src(path.src.html)
        .pipe(inject(gulp.src(['app/js/**/*.js', 'app/css/**/*.css'], {read: false}), { ignorePath: 'app/', addRootSlash: false }))
        .pipe(gulp.dest('app/'));
});

gulp.task('grid:build', function() {
    gulp.src(path.src.grid)
        .pipe(gulp.dest(path.build.css));
});

gulp.task('build', [
    'js:build',
    'style:build',
    'templatesHtml:build',
    'html:build',
    'grid:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);

