(function() {
  'use strict';

  angular
    .module('mixdataYeoman')
    .controller('MainController', MainController);

  /** @ngInject */
    function MainController(AuthService, $location) {
        var vm = this ;

        vm.validationName = validationName;
        function validationName(name, pass){
            AuthService.validationName(name, pass)
                .then(function() {
                    $location.path('/success')
                })
                .catch(function() {
                    vm.errorLog = true;
                });
        }

    }
})();
