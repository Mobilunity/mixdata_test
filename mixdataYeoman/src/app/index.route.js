(function() {
  'use strict';

  angular
    .module('mixdataYeoman')
    .config(routeConfig);

  function routeConfig($routeProvider,  $locationProvider) {
      $routeProvider.when('/', {
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'login'
      });

      $routeProvider.when('/success', {
          templateUrl: 'app/components/Success/success.html',
          controller: 'MainController',
          controllerAs: 'login'
      });

      $routeProvider.when('/password_change', {
          templateUrl: 'app/components/ChangePass/password_change.html',
          controller: 'ChangePassController',
          controllerAs: 'changePass'
      });

      $routeProvider.when('/forgot_pass', {
          templateUrl: 'app/components/ForgotPass/forgot_pass.html',
          controller: 'ForgotPassController',
          controllerAs: 'forgotPass'
      });

      $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
      });
  }

})();
