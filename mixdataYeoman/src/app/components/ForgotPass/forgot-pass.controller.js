(function() {
  'use strict';

  angular
    .module('mixdataYeoman')
    .controller('ForgotPassController', ForgotPassController);

  /** @ngInject */
    function ForgotPassController(AuthService, $location) {
        var vm = this ;

        vm.forgotValid = forgotValid;
        function forgotValid(username){
            AuthService.forgotValid(username)
                .then(function() {
                    $location.path('/password_change')
                })
                .catch(function() {
                    vm.validName = true;
                })
        }

    }
})();
