(function() {
  'use strict';

  angular
    .module('mixdataYeoman')
    .controller('ChangePassController', ChangePassController);

  /** @ngInject */
    function ChangePassController(AuthService, $location) {
        var vm = this ;

        vm.saveChange = saveChange;
        function saveChange(passNew, passRepeat){
            AuthService.saveChange(passNew, passRepeat)
                .then(function() {
                    $location.path('/success')
                })
                .catch(function() {
                    vm.repeatPasswordCheck = true;
                })
        }

    }
})();
