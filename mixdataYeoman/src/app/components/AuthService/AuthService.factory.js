(function() {
  'use strict';

    angular
        .module('mixdataYeoman')
        .factory('AuthService', AuthService);

    /** @ngInject */
    function AuthService($q,$timeout){

        var logData = {
            name: 'admin',
            mail: 'admin@mail.com',
            pass: '1234'
        };

        return {

            validationName: function validationName(username, password) {
                var q = $q.defer();

                $timeout(function() {
                    if (username === logData.name && password == logData.pass) {
                        q.resolve();
                    }
                    else if (username === logData.mail && password == logData.pass) {
                        q.resolve();
                    }
                    else {
                        q.reject();
                    }
                }, 100);

                return q.promise;
            },

            forgotValid: function forgotValid(username) {
                var q = $q.defer();

                $timeout(function() {
                    if (username === logData.name) {
                        q.resolve();
                    }
                    else if (username === logData.mail) {
                        q.resolve();
                    }
                    else {
                        q.reject();
                    }
                }, 100);

                return q.promise;
            },

            saveChange: function saveChange(passNew, passRepeat) {
                var q = $q.defer();

                $timeout(function() {
                    if (passNew === passRepeat && passNew != undefined && passRepeat != undefined) {
                        logData.pass = passNew;
                        q.resolve();
                    }
                    else {
                        q.reject();
                    }
                }, 100);

                return q.promise;
            }
        };
    }
})();
