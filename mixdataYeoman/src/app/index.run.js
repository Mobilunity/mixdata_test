(function() {
  'use strict';

  angular
    .module('mixdataYeoman')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
